package com.brainacad.oop.testpredefannotation.thirdtask;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by andriy on 01.04.17.
 */
public class Action {
    private Path path = Paths.get("file.txt");

    @MyPermission(userName = "Andriy")
    public void write2file(String array) {
        try(FileWriter fos = new FileWriter(path.toFile(), true)){
            fos.write(array);
        }catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    @MyPermission(userName = "Volodymyr")
    public String readFromFile() throws IOException {
        return new String(Files.readAllBytes(path));
    }
}
