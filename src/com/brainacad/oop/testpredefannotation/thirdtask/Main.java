package com.brainacad.oop.testpredefannotation.thirdtask;

import javax.lang.model.element.AnnotationValue;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;

/**
 * Created by andriy on 01.04.17.
 */
public class Main {
static String[] users = {"Volodymyr", "Andriy", "Olexandr"};
    public static void main(String[] args) {

        Action action = new Action();
        String array = new String("Contents of file. ");
        doWriteAction(action,array);
        System.out.println(doReadAction(action));

    }

    public static void doWriteAction(Action a, String array){
        Method m = null;
        try {
            m = a.getClass().getMethod("write2file", String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if(annotationProcessing(m,a)){
            a.write2file(array);
            System.out.println("Data has written to file");
        }else{
            System.err.println("Access denied");
        }
    }

    public static String doReadAction(Action a) {
        Method m = null;
        try {
            m = a.getClass().getMethod("readFromFile");
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        String string = null;
        if(annotationProcessing(m,a)){
            System.out.println("Data was red from file");
            try {
                string = a.readFromFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            System.err.println("Access denied");
        }
        return string;
    }

    private static boolean annotationProcessing(Method method, Action action){
        Class actionClass = action.getClass();
        Annotation annotation = method.getAnnotation(MyPermission.class);
        // proper way to get annotation value
        String annotationValue = method.getAnnotation(MyPermission.class).userName();
        boolean validUser = false;
        for(String userName : users) {
            if(annotationValue.equals(userName)){
                validUser = true;
                break;
            }
        }
        return validUser;
    }
}
