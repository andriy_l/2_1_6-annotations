package com.brainacad.oop.testpredefannotation.thirdtask;

@FunctionalInterface
public interface Doable {
    void justDoIt();
}
