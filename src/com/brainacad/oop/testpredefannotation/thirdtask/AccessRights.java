package com.brainacad.oop.testpredefannotation.thirdtask;

/**
 * Created by andriy on 01.04.17.
 */
public enum AccessRights {
    READ, WRITE;
}
