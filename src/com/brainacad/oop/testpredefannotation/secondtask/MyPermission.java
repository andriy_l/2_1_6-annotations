package com.brainacad.oop.testpredefannotation.secondtask;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD})
public @interface MyPermission {
    PermissionAction value() default PermissionAction.USER_READ;
}
