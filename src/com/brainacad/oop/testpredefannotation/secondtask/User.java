package com.brainacad.oop.testpredefannotation.secondtask;

import java.util.List;

public class User {
    private String name;
    private List<PermissionAction> permissionActions;

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", permissionActions=" + permissionActions +
                '}';
    }

    @MyPermission(PermissionAction.USER_READ)
    public List<PermissionAction> getPermissionActions() {
        return permissionActions;
    }
    @MyPermission(PermissionAction.USER_READ)
    public void setPermissionActions(List<PermissionAction> permissionActions) {
        this.permissionActions = permissionActions;
    }
}
