package com.brainacad.oop.testpredefannotation.secondtask;

public enum PermissionAction {
    USER_READ, USER_CHANGE, USER_WRITE;
}
