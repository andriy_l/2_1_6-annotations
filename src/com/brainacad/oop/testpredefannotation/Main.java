package com.brainacad.oop.testpredefannotation;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Main.findMax(1, 10, 20, 7, 100, 32);
        Main.findMax2(1, 10, 20, 7, 100, 32);
    }

    @Deprecated
    public static int findMax(int...array){
        Arrays.sort(array);
        return array[array.length-1];
    }

    @SafeVarargs
    public static<T> T findMax2(T...array){
        Arrays.sort(array);
        return array[array.length-1];
    }

    @SuppressWarnings("unchecked")
    public static void strangeCasting() {
        Set<String> stringSet = new HashSet();
    }
}
