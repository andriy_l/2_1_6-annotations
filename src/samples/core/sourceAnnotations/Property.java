package samples.core.sourceAnnotations;
import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD) // - meta annotations
@Retention(RetentionPolicy.SOURCE) // - meta annotations
public @interface Property
{
   String editor() default ""; 
}
