package samples.first;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
// @ before interface - annotation
public @interface BaseAction {
    int level();
    String sqlRequest();
}

