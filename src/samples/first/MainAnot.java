package samples.first;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by andriy on 29.03.17.
 */
public class MainAnot {
    public static void main(String[] args) {
        Class c = F.class;
        Method[] methods = c.getMethods();
        for (Method m: methods) {
            Annotation[] annotations = m.getAnnotations();
            for (Annotation a:
                 annotations) { // if RetentionPolicy.RUNTIME not empty
                System.out.println(a);
            }
        }
    }
}
