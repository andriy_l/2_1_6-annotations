package samples.first;
@BaseAction (  // after ( - go initializing parameters
               // for methods members of annotation
        level = 2,
        sqlRequest = "SELECT * FROM phonebook" // without parenthees () !!!
        // methods-members look like fields !!!
        // cannot contain throws
)
public class Base {
    public void doAction() {

        Class<Base> f = Base.class;

        BaseAction a = (BaseAction)f.getAnnotation(BaseAction.class);
        System.out.println(a.level());
        System.out.println(a.sqlRequest());
    }
}