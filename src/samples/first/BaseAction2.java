package samples.first;

// максимально довгий час життя анотації
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
@Retention(RetentionPolicy.RUNTIME)
public @interface BaseAction2 {
}
