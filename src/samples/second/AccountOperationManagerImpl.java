package samples.second;

/**
 * Created by andriy on 27.03.17.
 */
public class AccountOperationManagerImpl implements AccountOperationManager {
    @BankingAnnotation(securityLevel = SecurityLevelEnum.HIGH)
    public double depositInCash(int accountNumber, int amount) {
        // зарахування на депозит
        return 0; // stub
    }
    @BankingAnnotation(securityLevel = SecurityLevelEnum.HIGH)
    public boolean withdraw(int accountNumber, int amount) {
        // списання
        return true; // stub
    }
    @BankingAnnotation(securityLevel = SecurityLevelEnum.LOW)
    public boolean convert(double amount) {
        // конвертація
        return true; // stub
    }
    @BankingAnnotation
    public boolean transfer(int accountNumber, double amount) {
       // перевід
        return true; // stub
    }
}
